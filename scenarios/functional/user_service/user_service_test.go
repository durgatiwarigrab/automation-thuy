package user_service

import (
	"net/http"
	"testing"

	"bitbucket.org/thuynguyent/automation/swagger_clients/userservice/userservice/user"
	"github.com/myteksi/go/automation/gandalf/engine/glamdring/glamrand"
	"github.com/myteksi/go/automation/gandalf/engine/tdk/tdkhttp"
	"github.com/myteksi/go/automation/gandalf/engine/tdk/tdktag/tag"
)

const (
	binaryTag  = "UserService"
	featureTag = "UserService"
)

var (
	merchantGrabID    = "e16982a3-4178-4d51-90a5-9044c226fe3a"
	merchantGrabPayID = "214870033171457"
)

func TestUserService_GetUser_Unauthorized(t *testing.T) {
	cTracer := sTracer.NewCase(t, tag.TypeFunctional, "Test user service API: get user by GrabID").
		ServiceBinaries(binaryTag).
		Features(featureTag)

	resp, err := userServiceClient.User.GetUser_401(
		cTracer,
		user.NewGetUserParams().
			WithMsgID(glamrand.AlphaNumericString(32)).
			WithGrabID(&merchantGrabID),
	)
	cTracer.Require().NoError(err, "Get user should return 401")
	cTracer.Require().NotEmpty(resp, "Get user response should not be empty")
}

func TestUserService_GetUserByGrabID(t *testing.T) {
	cTracer := sTracer.NewCase(t, tag.TypeFunctional, "Test user service API: get user by GrabID").
		ServiceBinaries(binaryTag).
		Features(featureTag)

	resp, err := userServiceClient.User.GetUser_200(
		cTracer,
		user.NewGetUserParams().
			WithMsgID(glamrand.AlphaNumericString(32)),
		tdkhttp.OptSetHeader("X-Client-ID", "2"),
		tdkhttp.OptSetHeader("X-Access-Token", "test_token"),
		tdkhttp.OptSetQueryParam("GrabID", merchantGrabID),
	)
	cTracer.Require().NoError(err, "Get user should return 200")
	cTracer.Require().NotEmpty(resp, "Get user response should not be empty")
	cTracer.Require().NotEmpty(resp.Payload.EmailID)
	cTracer.Require().Equal("test_prakash_oct24@grabtaxi.com", *resp.Payload.EmailID)
	cTracer.Require().NotEmpty(resp.Payload.ContactNumber)
	cTracer.Require().Equal("+6587654321", *resp.Payload.ContactNumber)
}

func TestUserService_GetUserByGrabPayID(t *testing.T) {
	cTracer := sTracer.NewCase(t, tag.TypeFunctional, "Test user service API: get user by GrabPayID").
		ServiceBinaries(binaryTag).
		Features(featureTag)

	resp, err := userServiceClient.User.GetUser_200(
		cTracer,
		user.NewGetUserParams().
			WithMsgID(glamrand.AlphaNumericString(32)).
			WithGrabPayID(&merchantGrabPayID),
		tdkhttp.OptSetHeader("X-Client-ID", "2"),
		tdkhttp.OptSetHeader("X-Access-Token", "test_token"),
	)
	cTracer.Require().NoError(err, "Get user should return 200")
	cTracer.Require().NotEmpty(resp, "Get user response should not be empty")
	cTracer.Require().NotEmpty(resp.Payload.EmailID)
	cTracer.Require().Equal("test_prakash_oct24@grabtaxi.com", *resp.Payload.EmailID)
	cTracer.Require().NotEmpty(resp.Payload.ContactNumber)
	cTracer.Require().Equal("+6587654321", *resp.Payload.ContactNumber)
}

func TestUserService_GetUserResponseHeader(t *testing.T) {
	cTracer := sTracer.NewCase(t, tag.TypeFunctional, "Test user service API: get response header").
		ServiceBinaries(binaryTag).
		Features(featureTag)

	var h http.Header
	resp, err := userServiceClient.User.GetUser_200(
		cTracer,
		user.NewGetUserParams().
			WithMsgID(glamrand.AlphaNumericString(32)),
		tdkhttp.OptSetHeader("X-Client-ID", "2"),
		tdkhttp.OptSetHeader("X-Access-Token", "test_token"),
		tdkhttp.OptSetQueryParam("GrabID", merchantGrabID),
		tdkhttp.OptGetResponseHeader(&h), // pass the pointer to capture the response header
	)
	cTracer.Require().NoError(err, "Get user should return 200")
	cTracer.Require().NotEmpty(resp, "Get user response should not be empty")
	cTracer.Require().NotEmpty(h, "Response header should not be empty")
	cTracer.Require().NotEmpty(h["Content-Type"], "Content-Type header should not be empty")
}
