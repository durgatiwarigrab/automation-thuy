package user_service

import (
	"log"
	"os"
	"testing"

	"bitbucket.org/thuynguyent/automation/config"
	"bitbucket.org/thuynguyent/automation/swagger_clients/userservice/userservice"
	"github.com/myteksi/go/automation/gandalf/engine/glamdring/glamsubmit"
	"github.com/myteksi/go/automation/gandalf/engine/glamdring/glamsuite"
	"github.com/myteksi/go/automation/gandalf/engine/tdk/tdkconfig"
	"github.com/myteksi/go/automation/gandalf/engine/tdk/tdktag/tag"
	"github.com/myteksi/go/staples/logging/loggingapi"
)

const (
	logTag = "user-service-functional"
)

var (
	sTracer           *glamsuite.S
	userServiceClient *userservice.GrabpayUserIdentityService
	conf              config.Config
)

func TestMain(m *testing.M) {
	sTracer = glamsuite.NewWithFlag(logTag, "User Service Functional Tests", loggingapi.NewStdOut(), false, glamsuite.OptTestType(tag.TypeFunctional), glamsuite.OptCoreAPIUnderTest(tag.APIPaysiv2))
	conf = config.GetConfig()
	userServiceClient = userservice.NewClientWithHost(conf.UserServiceUrl)

	client := &config.Client{&conf}
	tdkconfig.InitializeWithClient(client)

	// Run and Exit
	exitCode := sTracer.Run(m)
	if postErr := glamsubmit.Post(sTracer); postErr != nil {
		log.Fatalf("TestMain : error in glamsubmit.Post() : %v", postErr)
	}
	os.Exit(exitCode)
}
