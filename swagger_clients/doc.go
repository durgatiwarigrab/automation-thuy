// Package swagger_clients provides go-swagger generated code for service clients
package swagger_clients

/*
The code generation lines for every single service client package goes below.
Client generation command template:
	./scgen.sh <PACKAGE_NAME> <GRABDOCS_DIST_SWAGGER_JSON_URL>

Please ensure that the latest distribute-able JSON has been generated (from the YML spec).
Naming convention: Use lowercase for package names, stripping all special characters, ie. pax-API-V3 --> paxapiv3
*/

// Define the commands for `go generate`
//go:generate ./scgen.sh userservice ../docs/user_service.yml
