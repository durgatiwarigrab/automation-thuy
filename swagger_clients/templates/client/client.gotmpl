{{ if .Copyright -}}// {{ comment .Copyright -}}{{ end }}

package {{ .Name }}

// Override : client/client.gotmpl
//
{{ template "gandalfHeader" }}
// Editing this file might prove futile when you re-run the swagger generate command

import (
  "net/http"
  "github.com/go-openapi/errors"
  "github.com/go-openapi/swag"
  "github.com/go-openapi/runtime"
  "github.com/go-openapi/validate"

  "github.com/myteksi/go/staples/logging/loggingapi"

  "github.com/go-openapi/strfmt"
  "github.com/myteksi/go/automation/gandalf/engine/glamdring/glamsuite"
  "github.com/myteksi/go/automation/gandalf/engine/tdk/tdkhttp"
  "github.com/myteksi/go/automation/gandalf/engine/tdk/tdktag/tag"

  {{ range .DefaultImports }}{{ printf "%q" .}}
  {{ end }}
  {{ range $key, $value := .Imports }}{{ $key }} {{ printf "%q" $value }}
  {{ end }}
)

// New creates a new {{ humanize .Name }} API client.
func New(transport runtime.ClientTransport, formats strfmt.Registry, apiName tag.APITag) *Client {
  return &Client{
    apiName: apiName,
    transport: transport,
    formats: formats,
  }
}

/*
Client {{ if .Summary }}{{ .Summary }}{{ if .Description }}

{{ .Description }}{{ end }}{{ else if .Description}}{{ .Description }}{{ else }}for {{ humanize .Name }} API{{ end }}
*/
type Client struct {
  apiName   tag.APITag
  transport runtime.ClientTransport
  formats   strfmt.Registry
}

{{ range .Operations }}
// {{ camelize .Name }} is the underlying operation for all {{ .Name }}_XXX methods
//=======================================
func (a *Client) {{ camelize .Name }}(cTracer glamsuite.EndpointLogger, params *{{ pascalize .Name }}Params{{ if .Authorized }}, authInfo runtime.ClientAuthInfoWriter{{end}}{{ if .HasStreamingResponse }}, writer io.Writer{{ end }}, opts ...tdkhttp.OperationModifierOption) (interface{}, error) {
  // TODO: Validate the params before sending
  if params == nil {
    params = New{{ pascalize .Name }}Params()
  }
  {{ $length := len .SuccessResponses }}
    // create RequestResponseLogger
    rrLogger := cTracer.Endpoint({{ printf "%q" .Method }}, {{ printf "%q" .Path }}).
        SetAPI(a.apiName)

    // specify client and set roundtripper
    gClient := http.DefaultClient
    gClient.Transport = tdkhttp.NewGandalfRoundTripper(rrLogger, opts...)

  // build operation object
  responseReader := &{{ pascalize .Name }}Reader{formats: a.formats{{ if .HasStreamingResponse }}, writer: writer{{ end }}}
  requestOperation := &runtime.ClientOperation{
    ID: {{ printf "%q" .Name }},
    Method: {{ printf "%q" .Method }},
    PathPattern: {{ printf "%q" .Path }},
    ProducesMediaTypes: {{ printf "%#v" .ProducesMediaTypes }},
    ConsumesMediaTypes: {{ printf "%#v" .ConsumesMediaTypes }},
    Schemes: {{ printf "%#v" .Schemes }},
    Params: params,
    Reader: responseReader,
    {{ if .Authorized }}AuthInfo: authInfo,{{ end}}
    Context: params.Context,
    Client: gClient,
  }

  // submit request
  response, err := a.transport.Submit(requestOperation)
  return response, err
}

{{ $parent := . }}
{{ range .Responses}}
/*
{{ pascalize $parent.Name }}_{{ .Code }} {{ if $parent.Summary }}{{ pluralizeFirstWord (humanize $parent.Summary) }}{{ if $parent.Description }}

{{ $parent.Description }}{{ end }}{{ else if $parent.Description}}{{ $parent.Description }}{{ else }}{{ humanize $parent.Name }} API{{ end }}
This operation returns *{{ pascalize .Name }} and a nil error if successful.
This operation returns nil and a non-nil error if unsuccessful.
*/
func (a *Client) {{ pascalize $parent.Name }}_{{ .Code }}(cTracer glamsuite.EndpointLogger, params *{{ pascalize $parent.Name }}Params{{ if $parent.Authorized }}, authInfo runtime.ClientAuthInfoWriter{{end}}{{ if $parent.HasStreamingResponse }}, writer io.Writer{{ end }}, opts ...tdkhttp.OperationModifierOption) (*{{ pascalize .Name }}, error) {
  // perform request
  response, err := a.{{ camelize $parent.Name }}(cTracer, params{{ if $parent.Authorized }}, authInfo{{ end }}{{ if $parent.HasStreamingResponse }}, writer{{ end }}, opts...)

  // return casted response if result type matches expected type
  if result, ok := response.(*{{ pascalize .Name }}); ok {
  		return result, nil
  } // else return error
  return nil, fmt.Errorf("expected %T but received %T : %v", &{{ pascalize .Name }}{}, response, err)
}
{{ end }}
//=======================================
{{ end }}

// SetTransport changes the transport on the client
func (a *Client) SetTransport(transport runtime.ClientTransport) {
  a.transport = transport
}
