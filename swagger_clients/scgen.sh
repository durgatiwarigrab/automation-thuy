#!/usr/bin/env bash

PACKAGE=$1
SWAGGER_DOC=$2
PACKAGE_FULL_PATH=$(dirname "${BASH_SOURCE}")/${PACKAGE}

if [[ ! -d "${PACKAGE_FULL_PATH}" ]]; then
    mkdir "${PACKAGE_FULL_PATH}"      # create new directory
else
    rm -rf "${PACKAGE_FULL_PATH}/*"   # remove all internal contents before regeneration (in case of conflict)
fi

# generate package
swagger generate client -C=./client_gen_config.yml -T=templates -f=$SWAGGER_DOC -m="$PACKAGE"models \
-c=$PACKAGE -t=$PACKAGE -r templates/copyright_header.txt

# add package to git
git add ${PACKAGE_FULL_PATH}

# finish
echo "gandalf-client : $PACKAGE : exit code $?"
