package config

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"path/filepath"
	"runtime"

	"github.com/myteksi/go/automation/gandalf/engine/tdk/tdkconfig"
)

const (
	configFile = "config.json"
	configUrl  = "https://api.myjson.com/bins/17ktr7"
)

// Config defines the configuration for automation project
type Config struct {
	UserServiceUrl string                `json:"user_service_url"`
	XmUrl          string                `json:"xm_url"`
	Frodo          tdkconfig.FrodoConfig `json:"frodo"`
}

// GetConfig loads from the configuration source and returns a Config object
func GetConfig() Config {
	var conf Config
	// Get config from url
	//FromUrl(configUrl, &conf)
	// Get config from file
	_, b, _, _ := runtime.Caller(0)
	dir := filepath.Dir(b)
	FromFile(filepath.Join(dir, configFile), &conf)
	return conf
}

// FromFile returns the JSON config from a file
func FromFile(filename string, config interface{}) error {
	jsonBlob, _ := ioutil.ReadFile(filename)
	if err := json.Unmarshal(jsonBlob, config); err != nil {
		log.Printf("[config.FromFileError] Error while unmarshaling config file")
		return err
	}
	return nil
}

// FromUrl returns the JSON config from a url
func FromUrl(url string, config interface{}) error {
	resp, err := http.Get(url)
	if err != nil {
		log.Printf("[config.FromUrlError] Error loading file from %s", url)
		return err
	}

	defer resp.Body.Close()
	var buf bytes.Buffer
	buf.ReadFrom(resp.Body)
	jsonBlob := buf.Bytes()
	if err := json.Unmarshal(jsonBlob, config); err != nil {
		log.Printf("[config.JSONUnmarshalError] Error while unmarshaling config file")
		return err
	}
	return nil
}
