package config

import (
	"encoding/json"
	"fmt"
)

const (
	frodoConfigKey = "tdkconfig_frodo"
)

type Client struct {
	Conf *Config
}

// GetObject is required method for Gandalf config client (tdkconfig.SitevarObjectClient)
func (c *Client) GetObject(key string, v interface{}) error {
	if key == frodoConfigKey {
		bytes, err := json.Marshal(c.Conf.Frodo)
		if err != nil {
			return fmt.Errorf("error while marshalling : %v", err)
		}
		err = json.Unmarshal(bytes, &v)
		if err != nil {
			return fmt.Errorf("error while unmarshalling : %v", err)
		}
		return nil
	}
	return fmt.Errorf("Only support frodo key at the moment")
}
